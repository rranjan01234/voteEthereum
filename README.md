# Voting for General Elections
-------------------------------

An Ethereum based blockchain web application implementing Voting for general Elections.

### Tools and softwares neccessary for starting this application
_____________________________________________________________
  a) Node.js and NPM<br/>
  b) Truffle and Truffle Ganache<br/>
  c) Metamask Wallet as a chrome or any browser extension.<br/>
_____________________________________________________________

### Demo video for starting this application ( Run "npm install" before following this video )
[![Demo Video](https://j.gifs.com/yozovP.gif)](https://www.youtube.com/watch?v=TIowp9w1xPA)
